README

This repo hosts an implementation of the cascade SVM algorithm proposed by Graf et al (2005) written in R. It is a parallel algorithm for training support vector machine classifiers. The intended use case of this code is to train SVMs on large data sets quickly using powerful commodity hardware available on Amazon AWS.

This is a public repository, but if you use this code, please cite it. 
BibTex:
@misc{Norberg2014,
  author = {Norberg, Robert},
  title = {Cascade Support Vector Machines},
  year = {2014},
  publisher = {BitBucket},
  journal = {BitBucket repository},
  howpublished = {\url{https://bitbucket.org/robertcnorberg/cascade-svms}},
  commit = {XXXXXXXXX_commit_XXXX_hash_#####}
}

No code contained in this repository is guaranteed in any way to produce useful, correct, or reliable results.

To contact the administrator of this repository, please email robertcnorberg@gmail.com with the phrase "cascade SVM repository" in the subject line.